import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { graphql, Link } from "gatsby"
import { Card, CardBody, CardSubtitle, Badge } from "reactstrap"
import Img from "gatsby-image"
import authors from "../util/authors"
import { slugify } from "../util/utilityFunctions"
import { DiscussionEmbed } from "disqus-react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faFacebookF,
  faTwitter,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons"

const SinglePost = ({ data, pageContext }) => {
  const post = data.markdownRemark.frontmatter
  const author = authors.find(x => x.name === post.author)
  const baseUrl = "https://movie-blog-gatsby.co.uk/"
  const disqusShortname = "movie-blog-gatsby"
  const disqusConfig = {
    config: {
      identifier: data.markdownRemark.id,
      title: post.title,
    },
  }

  return (
    <Layout
      pageTitle={post.header}
      postAuthor={author}
      authorImageFluid={data.file.childImageSharp.fluid}
    >
      <SEO title={post.title} />
      <Card className="my-card">
        <Img
          className="card-image-top"
          fluid={post.image.childImageSharp.fluid}
        />
        <CardBody>
          <CardSubtitle>
            <span className="text-info">{post.date}</span> by{" "}
            <span className="text-info">{post.author}</span>
          </CardSubtitle>
          <div
            dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
            className="my-post"
          />
          <ul className="post-tags">
            {post.tags.map(tag => (
              <li key={tag}>
                <Link to={`/tag/${slugify(tag)}`}>
                  <Badge color="primary" className="my-tags">
                    {tag}
                  </Badge>
                </Link>
              </li>
            ))}
          </ul>
          <Badge color="primary" className="text-uppercase my-rating-single">
            {post.rating}
          </Badge>
        </CardBody>
      </Card>
      <div className="comments-style">
        <h3 className="text-center">Share this post</h3>
        <div className="text-center social-share-links">
          <ul>
            <li>
              <a
                href={
                  "https://www.facebook.com/sharer/sharer.php?u=" +
                  baseUrl +
                  pageContext.slug
                }
                className="facebook"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faFacebookF} size="lg" />
              </a>
            </li>
            <li>
              <a
                href={
                  "https://www.twitter.com/share?url=" +
                  baseUrl +
                  pageContext.slug +
                  "&text=" +
                  post.title +
                  "&via" +
                  "twitterhandle"
                }
                className="twitter"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faTwitter} size="lg" />
              </a>
            </li>

            <li>
              <a
                href={
                  "https://www.linkedin.com/shareArticle?url=" +
                  baseUrl +
                  pageContext.slug
                }
                className="linkedin"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faLinkedinIn} size="lg" />
              </a>
            </li>
          </ul>
        </div>
        <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
      </div>
    </Layout>
  )
}

export const postQuery = graphql`
  query blogPostBySlug($slug: String!, $imageUrl: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        header
        author
        rating
        date(formatString: "MMM Do YYYY")
        tags
        image {
          childImageSharp {
            fluid(maxWidth: 700) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    file(relativePath: { eq: $imageUrl }) {
      childImageSharp {
        fluid(maxWidth: 300, maxHeight: 400) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default SinglePost
