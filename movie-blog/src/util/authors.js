const authors = [
  {
    name: "Morgan Booth",
    imageUrl: "morgan.jpg",
    bio:
      "Morgan Booth is a 30-year-old government politician who enjoys football, reading and binge-watching boxed sets. He is smart and inspiring, but can also be very stingy and a bit boring.         He is Cornish. He has a degree in philosophy, politics and economics. He has a severe phobia of ducks  ",
    facebook: "https://www.facebook.com/",
    twitter: "https://www.twitter.com/",
    instagram: "https://www.instagram.com/",
    google: "https://www.google.com/",
    linkedIn: "https://www.linkedin.com/",
  },
  {
    name: "Mia Patterson",
    imageUrl: "mia.jpg",
    bio:
      "Mia Patterson is a 28-year-old scientific researcher who enjoys Rubix cube, glamping and escapology. She is exciting and creative, but can also be very lazy and a bit impatient.She is Australian. She has a post-graduate degree in chemistry.",
    facebook: "https://www.facebook.com/",
    twitter: "https://www.twitter.com/",
    instagram: "https://www.instagram.com/",
    google: "https://www.google.com/",
    linkedIn: "https://www.linkedin.com/",
  },
  {
    name: "Sonja Hatakka",
    imageUrl: "sonja.jpg",
    bio:
      "She is Finnish. She started studying computing at college but never finished the course. She is allergic to soy. She is currently married to Filip Igor Palmer. Filip is 6 years older than her and works as a chef.",
    facebook: "https://www.facebook.com/",
    twitter: "https://www.twitter.com/",
    instagram: "https://www.instagram.com/",
    google: "https://www.google.com/",
    linkedIn: "https://www.linkedin.com/",
  },
]

module.exports = authors
