import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import authors from "../util/authors"
import { Row, Card, CardText, CardTitle, CardBody, Button } from "reactstrap"
import MiaImage from "../images/mia.jpg"
import MorganImage from "../images/morgan.jpg"
import SonjaImage from "../images/sonja.jpg"

import { slugify } from "../util/utilityFunctions"

const TeamPage = () => (
  <Layout pageTitle=" Our Team">
    <SEO title="Our Team" />
    <Row className="md-4  my-authors">
      <div className="col-md-3">
        <img
          src={MiaImage}
          style={{ maxWidth: "100%", maxHeight: "100%" }}
          alt="Mia profile"
          className="my-card"
        />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: "100%" }} className="my-card">
          <CardBody>
            <CardTitle className="my-title">{authors[1].name}</CardTitle>
            <CardText className="my-post">{authors[1].bio}</CardText>
            <Button
              className="text-uppercase my-button-author"
              href={`/author/${slugify(authors[1].name)}`}
            >
              {" "}
              View Posts
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
    <Row className="md-4  my-authors">
      <div className="col-md-3">
        <img
          src={MorganImage}
          style={{ maxWidth: "100%", maxHeight: "100%" }}
          alt="Morgan profile"
          className="my-card"
        />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: "100%" }} className="my-card">
          <CardBody>
            <CardTitle className="my-title">{authors[0].name}</CardTitle>
            <CardText className="my-post">{authors[0].bio}</CardText>
            <Button
              className="text-uppercase my-button-author"
              href={`/author/${slugify(authors[0].name)}`}
            >
              {" "}
              View Posts
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>

    <Row className="md-4  my-authors">
      <div className="col-md-3">
        <img
          src={SonjaImage}
          style={{ maxWidth: "100%", maxHeight: "100%" }}
          alt="Sonja profile"
          className="my-card"
        />
      </div>
      <div className="col-md-8">
        <Card style={{ minHeight: "100%" }} className="my-card">
          <CardBody>
            <CardTitle className="my-title">{authors[2].name}</CardTitle>
            <CardText className="my-post">{authors[2].bio}</CardText>
            <Button
              className="text-uppercase my-button-author"
              href={`/author/${slugify(authors[2].name)}`}
            >
              {" "}
              View Posts
            </Button>
          </CardBody>
        </Card>
      </div>
    </Row>
  </Layout>
)

export default TeamPage
