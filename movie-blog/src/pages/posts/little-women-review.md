---
title: "little-women-review"
header: "Little Women Review"
date: 2019-12-26 07:00:00
author: "Morgan Booth"
image: ../../images/little-women.jpg
rating: "8/10"
tags:
  - Coming-of-age
  - Drama
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus feugiat in ante metus dictum. Morbi non arcu risus quis. Iaculis urna id volutpat lacus laoreet non curabitur. In nisl nisi scelerisque eu ultrices vitae auctor. Elementum nisi quis eleifend quam adipiscing. Erat nam at lectus urna duis convallis convallis. Volutpat maecenas volutpat blandit aliquam etiam. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Porttitor eget dolor morbi non arcu risus quis varius quam. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. In nulla posuere sollicitudin aliquam ultrices sagittis orci. Ultricies leo integer malesuada nunc.

Amet tellus cras adipiscing enim. Pulvinar neque laoreet suspendisse interdum consectetur. Aliquet nibh praesent tristique magna sit amet purus gravida. Orci eu lobortis elementum nibh tellus molestie nunc. Scelerisque varius morbi enim nunc faucibus. Feugiat vivamus at augue eget arcu dictum varius duis at. Arcu non sodales neque sodales ut etiam sit. Volutpat sed cras ornare arcu dui vivamus arcu felis bibendum. Sed libero enim sed faucibus turpis in. Purus faucibus ornare suspendisse sed. Ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Eu feugiat pretium nibh ipsum consequat nisl vel. Senectus et netus et malesuada. Blandit aliquam etiam erat velit scelerisque in dictum non. Leo vel fringilla est ullamcorper eget nulla facilisi etiam. Nam aliquam sem et tortor consequat id porta nibh venenatis. Nunc sed blandit libero volutpat sed cras ornare. Arcu non sodales neque sodales ut etiam sit.
