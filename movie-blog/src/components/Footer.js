import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faFacebook,
  faTwitter,
  faInstagram,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons"

const Footer = () => (
  <div className="site-footer">
    <h1 className="text-center">Movie Blog</h1>
    <p className="text-center">Follow us!</p>
    <div className="footer-social-links">
      <ul className="social-links-list ">
        <li>
          <a
            href="https://www.facebook.com"
            target="_blank"
            rel="noopener noreferrer"
            className="facebook my-social"
          >
            <FontAwesomeIcon icon={faFacebook} size="lg" />
          </a>
        </li>
        <li>
          <a
            href="https://www.twitter.com"
            target="_blank"
            rel="noopener noreferrer"
            className="twitter my-social"
            style={{ width: "100%" }}
          >
            <FontAwesomeIcon icon={faTwitter} size="lg" />
          </a>
        </li>
        <li>
          <a
            href="https://www.instagram.com"
            target="_blank"
            rel="noopener noreferrer"
            className="instagram my-social"
          >
            <FontAwesomeIcon icon={faInstagram} size="lg" />
          </a>
        </li>

        <li>
          <a
            href="https://www.linkedIn.com"
            target="_blank"
            rel="noopener noreferrer"
            className="linkedin my-social"
          >
            <FontAwesomeIcon icon={faLinkedin} size="lg" />
          </a>
        </li>
      </ul>
    </div>
  </div>
)

export default Footer
