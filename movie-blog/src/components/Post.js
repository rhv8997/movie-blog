import React from "react"
import { Link } from "gatsby"
import {
  Badge,
  Card,
  CardTitle,
  CardText,
  CardSubtitle,
  CardBody,
} from "reactstrap"
import Img from "gatsby-image"
import { slugify } from "../util/utilityFunctions"
const Post = ({ author, slug, date, body, fluid, tags, header, rating }) => {
  return (
    <Card className="my-card">
      <Link to={slug}>
        <Img className="card-image-top" fluid={fluid} />
      </Link>
      <CardBody>
        <CardTitle className="my-title">
          <Link className="my-link" to={slug}>
            {header}
          </Link>
          <Badge color="primary" className="text-uppercase my-rating">
            {rating}
          </Badge>
        </CardTitle>
        <CardSubtitle>
          <span className="text-info">{date}</span> by{" "}
          <span className="text-info"> {author}</span>
        </CardSubtitle>
        <CardText className="my-post"> {body} </CardText>
        <ul className="post-tags">
          {tags.map(tag => (
            <li key={tag}>
              <Link to={`/tag/${slugify(tag)}`}>
                <Badge color="primary" className="text-uppercase my-tags">
                  {tag}
                </Badge>
              </Link>
            </li>
          ))}
        </ul>
        <Link
          to={slug}
          className="btn btn-outline-primary float-right my-button"
        >
          Read more
        </Link>
      </CardBody>
    </Card>
  )
}

export default Post
