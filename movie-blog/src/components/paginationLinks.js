import React from "react"
import { Pagination, PaginationItem, PaginationLink } from "reactstrap"
const PaginationLinks = ({ currentPage, numberOfPages }) => {
  const isFirst = currentPage === 1
  const isLast = currentPage === numberOfPages
  const previousPage =
    currentPage - 1 === 1 ? `/` : `/page/` + (currentPage - 1).toString()
  const nextPage = `/page/` + (currentPage + 1).toString()
  return (
    <Pagination aria-label="Page navigation example">
      {isFirst ? (
        <PaginationItem disabled className="pages-block">
          <PaginationLink previous href="/" className="pages-link">
            {" "}
            {"<<"}{" "}
          </PaginationLink>
        </PaginationItem>
      ) : (
        <PaginationItem className="pages-block">
          <PaginationLink previous href={previousPage} className="pages-link">
            {"<<"}{" "}
          </PaginationLink>
        </PaginationItem>
      )}

      {Array.from({ length: numberOfPages }, (_, i) =>
        currentPage === i + 1 ? (
          <PaginationItem
            active
            key={`page-number${i + 1}`}
            className="pages-block"
          >
            <PaginationLink
              href={`/${i === 0 ? "" : "page/" + (i + 1)}`}
              className="pages-link"
            >
              {i + 1}
            </PaginationLink>
          </PaginationItem>
        ) : (
          <PaginationItem key={`page-number${i + 1}`} className="pages-block">
            <PaginationLink
              href={`/${i === 0 ? "" : "page/" + (i + 1)}`}
              className="pages-link"
            >
              {i + 1}
            </PaginationLink>
          </PaginationItem>
        )
      )}

      {isLast ? (
        <PaginationItem disabled className="pages-block">
          <PaginationLink next href={nextPage} className="pages-link">
            {">>"}{" "}
          </PaginationLink>
        </PaginationItem>
      ) : (
        <PaginationItem className="pages-block">
          <PaginationLink next href={nextPage} className="pages-link">
            {">>"}{" "}
          </PaginationLink>
        </PaginationItem>
      )}
    </Pagination>
  )
}

export default PaginationLinks
